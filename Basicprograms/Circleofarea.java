package Basicprograms;
import java.util.Scanner;

public class Circleofarea {
	public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double radius, diameter, circumference, area;

        System.out.print("Radius: ");
        radius = sc.nextDouble();
         diameter = 2 * radius;
        System.out.println("Diameter: " + diameter);
        circumference = 2 * Math.PI * radius;
        System.out.println("Circumference: " + circumference);
        area = Math.PI * radius * radius;
        System.out.println("Area: " + area);

    }
}



